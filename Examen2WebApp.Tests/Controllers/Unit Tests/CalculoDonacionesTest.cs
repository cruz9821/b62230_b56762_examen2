﻿using Examen2WebApp.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Examen2WebApp.Tests.Controllers.Unit_Tests
{
    [TestClass]
    public class CalculoDonacionesTest
    {
        [TestMethod]
        public void CalculoDonacionSinBono()
        {
            CalculoDonaciones calculoDonaciones = new CalculoDonaciones();
            double donacion = calculoDonaciones.CalcularDonacion("Mentoria Sin Bono", "No Doctorado", "No CompEInf", "Masculino", "Masculino");
            Assert.AreEqual(donacion, CalculoDonaciones.MIN_DONACION);
        }


        [TestMethod]
        public void CalculoMaximaDonacion()
        {
            CalculoDonaciones calculoDonaciones = new CalculoDonaciones();
            double donacion = calculoDonaciones.CalcularDonacion("Grupos de Investigación", "Doctorado", "Computación e Informática", "Femenino", "Masculino");
            double maximaDonacion = CalculoDonaciones.MIN_DONACION;
            maximaDonacion += maximaDonacion * CalculoDonaciones.BONO_INVESTIGACION;
            maximaDonacion += maximaDonacion * CalculoDonaciones.BONO_DOCTORADO;
            maximaDonacion += maximaDonacion * CalculoDonaciones.BONO_EST_COMP_INF;
            maximaDonacion += maximaDonacion * CalculoDonaciones.BONO_PARTIPANTE_MUJER;
            Assert.AreEqual(donacion, maximaDonacion);
        }

    }
}
