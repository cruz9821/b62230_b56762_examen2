﻿CREATE TABLE [dbo].[Mentoria]
(
	[CedulaDocente] VARCHAR(50) NOT NULL,
	[CedulaEstudiante] VARCHAR(50) NOT NULL,
	[AreaDeMentoriaId] INT NOT NULL,
	[Estado] VARCHAR(50) NOT NULL,
	[Donacion] FLOAT DEFAULT 0,
	PRIMARY KEY (CedulaDocente, CedulaEstudiante,AreaDeMentoriaId),
	FOREIGN KEY (CedulaDocente)
		REFERENCES Docente(Cedula),
	FOREIGN KEY (CedulaEstudiante)
		REFERENCES Estudiante(Cedula),
	FOREIGN KEY (AreaDeMentoriaId)
		REFERENCES AreaDeMentoria(Id)
)
