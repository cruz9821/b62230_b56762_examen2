﻿CREATE TABLE [dbo].[Estudiante]
(
	[Cedula] VARCHAR(50) NOT NULL PRIMARY KEY,
	[Carne] VARCHAR(50) NOT NULL,
	[Nombre] VARCHAR(100) NOT NULL,
	[Apellidos] VARCHAR(100) NOT NULL,
	[Carrera] VARCHAR(150) NOT NULL,
	[Enfasis] VARCHAR(150),
	[Genero] VARCHAR(20)
)

