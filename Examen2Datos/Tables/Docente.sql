﻿CREATE TABLE [dbo].[Docente]
(
	[Cedula] VARCHAR(50) NOT NULL PRIMARY KEY,
	[Nombre] VARCHAR(100) NOT NULL,
	[Apellidos] VARCHAR(100) NOT NULL,
	[Carrera] VARCHAR(150) NOT NULL,
	[GradoAcademico] VARCHAR(100),
	[Genero] VARCHAR(20)
)