﻿CREATE TABLE [dbo].[AreaDeMentoria]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[Nombre] VARCHAR(200) NOT NULL,
	[Descripcion] VARCHAR(600)
)
