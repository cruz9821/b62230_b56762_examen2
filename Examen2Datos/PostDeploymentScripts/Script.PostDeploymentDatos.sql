﻿
/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
USE Examen2Datos;

-- Borrar datos de las tablas
DELETE FROM AreaDeMentoria;
DELETE FROM Docente;
DELETE FROM Estudiante;

-- Annadir datos de prueba
INSERT INTO AreaDeMentoria(Nombre)
VALUES ('Estudios de Posgrado'), 
       ('Pasantías Profesionales'),
       ('Pasantías Academicas'),
       ('Grupos de Investigación')
      

INSERT INTO Docente(Cedula, Nombre, Apellidos, Carrera, Genero, GradoAcademico)
VALUES ('1411-1311', 'Brenda', 'Grau', 'Ingeniería Eléctrica', 'Femenino', 'Licenciatura'),
       ('1222-2452', 'Eloisa', 'Cornejo', 'Computación e Informática', 'Femenino', 'Doctorado'),
       ('5323-3343', 'Alfredo', 'Manso', 'Computación e Informática', 'Masculino', 'Maestría'),
       ('2574-7834', 'Oliver', 'Ocaña', 'Administración Pública', 'Masculino', 'Maestría'),
       ('2456-3344', 'Pedro', 'Montaño', 'Ingeniería Eléctrica', 'Masculino', 'Doctorado'),
       ('5553-3353', 'Aurelia ', 'Betancor', 'Administración Pública', 'Femenino', 'Maestría')

INSERT INTO Estudiante(Cedula, Carne, Nombre, Apellidos, Carrera, Genero)
VALUES ('3455-3563', 'B67355', 'Izaskun', 'Novo', 'Computación e Informática', 'Masculino'),
       ('3466-2355', 'B65746', 'Davinia', 'Yañez', 'Administración Pública', 'Femenino'),
       ('2356-2366', 'B56783', 'Debora', 'Quintana', 'Administración Pública', 'Femenino'),
       ('2467-6422', 'B84677', 'Amaia', 'Plaza', 'Computación e Informática', 'Femenino'),
       ('3677-3477', 'B89965', 'Jose Andres', 'Padilla', 'Ingeniería Eléctrica', 'Masculino'),
       ('3777-3467', 'B84562', 'Tomas', 'Herraiz', 'Ingeniería Eléctrica', 'Masculino')

INSERT INTO Mentoria(CedulaDocente, CedulaEstudiante, AreaDeMentoriaId, Estado, Donacion)
VALUES ('1411-1311', '2467-6422', 2, 'Pendiente', 0),
       ('2456-3344', '2467-6422', 1, 'Pendiente',0),
       ('2574-7834', '3777-3467', 2, 'Pendiente',0),
       ('5323-3343', '3466-2355', 3, 'Pendiente',0),
       ('1411-1311', '3455-3563', 4, 'Pendiente',0),
       ('1411-1311', '3677-3477', 4, 'Pendiente',0)


