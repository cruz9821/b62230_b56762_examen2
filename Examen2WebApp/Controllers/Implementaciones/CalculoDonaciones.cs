﻿using Examen2WebApp.Controllers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Examen2WebApp.Controllers
{
    public class CalculoDonaciones : ICalculoDonaciones
    {
        public static double MIN_DONACION = 50; // dolares
        public static double BONO_INVESTIGACION = 0.10;
        public static double BONO_DOCTORADO = 0.10;
        public static double BONO_EST_COMP_INF = 0.05; // bono estudiante de Computación e Informática
        public static double BONO_PARTIPANTE_MUJER = 0.10;
        
        public double CalcularDonacion(string areaDeMentoria, string gradoDocente, string carreraEstudiante, string generoDocente, string generoEstudiante)
        {
            double donacion = MIN_DONACION;

            if (areaDeMentoria == "Grupos de Investigación") // bono investigacion
            {
                donacion += donacion * BONO_INVESTIGACION;
            }
            if (gradoDocente == "Doctorado") // bono doctorado
            {
                donacion += donacion * BONO_DOCTORADO;
            }
            if (carreraEstudiante == "Computación e Informática") // bono estudiante de Computación e Informática
            {
                donacion += donacion * BONO_EST_COMP_INF;
            }
            if (generoDocente == "Femenino" || generoEstudiante == "Femenino") // bono participante mujer
            {
                donacion += donacion * BONO_PARTIPANTE_MUJER;
            }
            return donacion;
        }


    }
}