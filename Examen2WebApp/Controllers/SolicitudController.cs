﻿using Examen2WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Examen2WebApp.Controllers.Interfaces;

namespace Examen2WebApp.Controllers
{
    public class SolicitudController : Controller
    {
        private Examen2DatosEntities db = new Examen2DatosEntities();

        private ICalculoDonaciones servicioDonaciones = new CalculoDonaciones();

        public List<Docente> docentes;
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            docentes = db.Docentes.ToList();
            ViewBag.docentes = docentes;
        }

        // GET: Solicitud
        public ActionResult Index()
        {
            return View();
        }

        public void CalcularDonacionMentoria(Mentoria mentoria)
        {
            if (mentoria.Estado == "Aceptada" && (mentoria.Donacion == 0 || mentoria.Donacion == null))
            {
                mentoria.Donacion = servicioDonaciones.CalcularDonacion(mentoria.AreaDeMentoria.Nombre, mentoria.Docente.GradoAcademico, mentoria.Estudiante.Carrera, mentoria.Docente.Genero, mentoria.Estudiante.Genero);
                GuardarMentoria(mentoria);
            }
        }

        public void GuardarMentoria(Mentoria mentoria)
        {
            db.Entry(mentoria).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public ActionResult RequestsList(string id)
        {
            
            List<Mentoria> mentorias = db.Mentorias.Where(m => m.CedulaDocente == id).ToList();
            if (mentorias == null)
            {
                return HttpNotFound();
            }
            foreach (var mentoria in mentorias)
            {
                CalcularDonacionMentoria(mentoria);
            }
            return View(mentorias);

        }

        public ActionResult AcceptRequest(string cedDoc, string cedEst, int areaId)
        {
            Mentoria mentoria = db.Mentorias.FirstOrDefault(m => m.CedulaDocente == cedDoc && cedEst == m.CedulaEstudiante && m.AreaDeMentoriaId == areaId);
            if (mentoria != null)
            {
                mentoria.Estado = "Aceptada";
                GuardarMentoria(mentoria);
            }
            return RedirectToAction("RequestsList", new { id = cedDoc });
        }

    }
}