﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen2WebApp.Controllers.Interfaces
{
    public interface ICalculoDonaciones
    {
        double CalcularDonacion(string areaDeMentoria, string gradoDocente, string carreraEstudiante, string generoDocente, string generoEstudiante);
    }
}
