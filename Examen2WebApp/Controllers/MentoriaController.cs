﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Examen2WebApp.Models;

namespace Examen2WebApp.Controllers
{
    public class MentoriaController : Controller
    {
        private Examen2DatosEntities db = new Examen2DatosEntities();

        // GET: Mentoria
        public ActionResult Index()
        {
            var mentorias = db.Mentorias.Include(m => m.AreaDeMentoria).Include(m => m.Docente).Include(m => m.Estudiante);
            return View(mentorias.ToList());
        }

        // GET: Mentoria/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mentoria mentoria = db.Mentorias.Find(id);
            if (mentoria == null)
            {
                return HttpNotFound();
            }
            return View(mentoria);
        }

        // GET: Mentoria/Create
        public ActionResult Create()
        {
            ViewBag.AreaDeMentoriaId = new SelectList(db.AreaDeMentorias, "Id", "Nombre");
            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre");
            ViewBag.CedulaEstudiante = new SelectList(db.Estudiantes, "Cedula", "Carne");
            return View();
        }

        // POST: Mentoria/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CedulaDocente,CedulaEstudiante,AreaDeMentoriaId,Estado")] Mentoria mentoria)
        {
            if (ModelState.IsValid)
            {
                mentoria.Donacion = 0;
                mentoria.Estado = "Pendiente";
                db.Mentorias.Add(mentoria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AreaDeMentoriaId = new SelectList(db.AreaDeMentorias, "Id", "Nombre", mentoria.AreaDeMentoriaId);
            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre", mentoria.CedulaDocente);
            ViewBag.CedulaEstudiante = new SelectList(db.Estudiantes, "Cedula", "Carne", mentoria.CedulaEstudiante);
            return View(mentoria);
        }

        // GET: Mentoria/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mentoria mentoria = db.Mentorias.Find(id);
            if (mentoria == null)
            {
                return HttpNotFound();
            }
            ViewBag.AreaDeMentoriaId = new SelectList(db.AreaDeMentorias, "Id", "Nombre", mentoria.AreaDeMentoriaId);
            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre", mentoria.CedulaDocente);
            ViewBag.CedulaEstudiante = new SelectList(db.Estudiantes, "Cedula", "Carne", mentoria.CedulaEstudiante);
            return View(mentoria);
        }

        // POST: Mentoria/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CedulaDocente,CedulaEstudiante,AreaDeMentoriaId,Estado")] Mentoria mentoria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mentoria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AreaDeMentoriaId = new SelectList(db.AreaDeMentorias, "Id", "Nombre", mentoria.AreaDeMentoriaId);
            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre", mentoria.CedulaDocente);
            ViewBag.CedulaEstudiante = new SelectList(db.Estudiantes, "Cedula", "Carne", mentoria.CedulaEstudiante);
            return View(mentoria);
        }

        // GET: Mentoria/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mentoria mentoria = db.Mentorias.Find(id);
            if (mentoria == null)
            {
                return HttpNotFound();
            }
            return View(mentoria);
        }

        // POST: Mentoria/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Mentoria mentoria = db.Mentorias.Find(id);
            db.Mentorias.Remove(mentoria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
